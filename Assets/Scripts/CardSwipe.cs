using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardSwipe : MonoBehaviour
{
    [SerializeField] private GameObject uiPanel;

    private int cardWidth;
    [SerializeField] private int cardPadding;

    public float shift;
    
    
    [SerializeField] private RectTransform[] cards;


    void InitCards()
    { 
        // neighbor cards not shown
        // only current card and left/right paddings 
        cardWidth = Screen.width - cardPadding * 2;
    }


    void ShiftCardsArrayRight()
    {
        RectTransform saved = cards[cards.Length-1];

        for (int i = cards.Length-1; i > 0; i--)
            cards[i] = cards[i - 1];

        cards[0] = saved;
    }
    
    
    void ShiftCardsArrayLeft()
    {
        RectTransform saved = cards[0];

        for (int i = 0; i < cards.Length-1; i++)
            cards[i] = cards[i + 1];

        cards[cards.Length-1] = saved;
    }
    

    void DrawCards()
    {
        //arrange cards positions according current animation shift state
        
        for(int i=0; i<cards.Length; i++)
        {
            cards[i].sizeDelta = new Vector2( cardWidth, 100 );
            
            float index =  (i+shift) % cards.Length - 1;
            
            cards[i].localPosition = new Vector3((cardWidth+cardPadding)*index, 0, 0 );
        }        
    }


    IEnumerator ShiftCards(int shift_direction)
    {
        // do animation in 50 frames
        // ... or do it in your own way
        int frames_to_do = 50;
        
        // animation step per frame
        float step = (float)shift_direction / frames_to_do;

        // animate
        for (int i = 0; i < frames_to_do; i++)
        {
            shift += step;
            DrawCards();

            yield return null;
        }

        // reassign array
        if (shift_direction == 1)
            ShiftCardsArrayRight();
        else
            ShiftCardsArrayLeft();
        
        //reset shift to zero & redraw cards
        shift = 0;
        DrawCards();
        
        //skip one more frame
        yield return null;
    }


    private void Start()
    {
        // setup card width and maybe somth more...
        InitCards();
        
        // first card always drawn beside left part of screen
        // we need to show first as second at center
        ShiftCardsArrayRight();
        DrawCards();
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
            StartCoroutine(ShiftCards(1));
        
        if (Input.GetKeyDown(KeyCode.LeftArrow))
            StartCoroutine(ShiftCards(-1));
    }
}
